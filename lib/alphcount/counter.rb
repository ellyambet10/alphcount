module Alphcount
  class Counter
    def alph_letters (alphabet = "")
      #alphabet.downcase!
      alph_count = {}
      alph_count.default = 0
      alphabet.each_char {|char| alph_count[char] += 1}

      for alph in ("a".."z").each
        next if alph_count[alph] == 0
        puts "#{alph}:#{alph_count[alph]}"
      end
    end
  end
end
