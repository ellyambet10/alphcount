
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "alphcount/version"

Gem::Specification.new do |spec|
  spec.name          = "alphcount"
  spec.version       = Alphcount::VERSION
  spec.authors       = ["Elly Ambet"]
  spec.email         = ["ellyambet10@gmail.com"]

  spec.summary       = "Letter Frequency Counter"
  spec.description   = "This gem takes the record of each and every letter in the alphabet."
  spec.homepage      = "http://rubygems.org/gems/alphcount"
  spec.license       = "MIT"

  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "RubyGems.org"

    spec.metadata["homepage_uri"] = spec.homepage
    spec.metadata["source_code_uri"] = "https://github.com/elibiz443/alphcount"
    spec.metadata["changelog_uri"] = "https://github.com/elibiz/alphcount/CHANGELOG.md"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.17"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.0"
  spec.add_development_dependency "codelog", "~> 0.8.0"
end
